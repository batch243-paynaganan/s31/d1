/* 
    CREATE A ROUTE
        '/greeting'
*/

const http = require("http");
const port = 4000;

const server = http.createServer((request, response) => {
    if (request.url == "/greeting") {
      response.writeHead(200, { "Content-Type": "text/plain" });
      response.end("Hello Again!!!")
    // accessing the 'homepage' route returns a message of "this is homepage."
    } else if(request.url == '/homepage'){
        response.writeHead(200,{'Content-Type':'text/plain'});
        response.end("Welcome Home!")
    // all other routes will return a message of page not available
    }else{
        response.writeHead(400,{'Content-Type':'text/plain'});
        response.end("Page not available")
    }
  }).listen(port);

console.log(`Listening to port: ${port}`);
/* 
  server.listen(port)
*/
