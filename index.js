// use the require directive to load the http module of node js
// a module is software component of part of a program that contains one or more routines
// the http module lets noed.js transfer data using the http(hypertext tranfer protocol)
// http is a protocol that allows fetching of resources such as html documents
// clients(browser) and server(nodeJS/expressJS applications) communicate by exchanging individual messages

// REQUEST - messages sent by the client (usually web browser)
// RESPONSES - messages sent by the server as an asnwer to the client

let http = require("http")


// CREATE A SERVER
/* 
    The http module has a createServer()method that accepts a function as an argument and allows server creation.
    the arguments passed in the function are request & response obj (data type) that contain methods that allows us to recieve requests from the client and send the response back
    using the module's createServer()method, we can create an HTTP server that listens to the requests on a specified port and gives back to the client
*/
// Define the port number that the server will be listening to
http.createServer(function(request, response){
    response.writeHead(200,{'Content-Type':'text/plain'});
    response.end('Hello Kitty!')
}).listen(4000)

console.log('Server is running at localhost:4000')

// a port is virtual point where network connections start and end.


// send a response back to client
// use the writeHead()method
// Set a status code for the response - a 200 - ok

// inputting the command tells our devices to run node js
// node index.js

// install nodemon
// npm install -g nodemon

// to check if nodemon is already installed
// nodemon -v


/* 
    IMPORTANT NOTE:
    installing the package will allow the server to automatically restart when files have been change or updated
    '-g' refers to a global installation where the current version of the package/dependency will be installed locally on our device allowing us to use nodemon on any project.
*/
